import AOS from 'aos';
import 'bootstrap';

import '../scss/index.scss';

AOS.init();

$('#alert').on('click', () => {
  alert('jQuery works!');
});

function toggleBackgroundNavbar() {
  if (window.pageYOffset > 0) {
    $('#navbar').addClass('navbar-scroll-active');
    $('#navbar').removeClass('navbar-scroll-deactive');
  } else {
    $('#navbar').removeClass('navbar-scroll-active');
    $('#navbar').addClass('navbar-scroll-deactive');
  }
}

toggleBackgroundNavbar();

document.addEventListener('scroll', toggleBackgroundNavbar);
